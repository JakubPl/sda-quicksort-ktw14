package pl.sda;

import java.util.Arrays;

public class Quicksort {
    private static int licznik = 0;

    public static void main(String[] args) {
        int[] arrayToSort = {6, 14, 11, 12, 13, 10, 9, 8, 7, 15, 5, 4, 3, 0, 1, 2};
        quicksort(arrayToSort, 0, arrayToSort.length - 1);
        System.out.println(Arrays.toString(arrayToSort));
        System.out.println("Ilosc zamian:" + licznik);
    }

    public static void quicksort(int[] arrayToSort, int start, int end) {
        if (start < end) {
            int finalPiviotLocation = partitionArray(arrayToSort, start, end);
            quicksort(arrayToSort, start, finalPiviotLocation - 1); //lewa czesc
            quicksort(arrayToSort, finalPiviotLocation + 1, end); //prawa czesc
        }
    }

    private static int partitionArray(int[] arrayToSort, int start, int end) {
        int piviot = selectPiviot(start, end);
        int piviotValue = arrayToSort[piviot];
        swap(arrayToSort, start, piviot);
        //TODO: zamiana osi z lewą strona
        int lastSwappedIndex = start;
        for (int currentElemtnIndex = start + 1; currentElemtnIndex < arrayToSort.length; currentElemtnIndex++) {
            int currentElementToCompare = arrayToSort[currentElemtnIndex];
            if (piviotValue > currentElementToCompare) {
                lastSwappedIndex++;
                licznik++;
                swap(arrayToSort, currentElemtnIndex, lastSwappedIndex);
            }
        }
        swap(arrayToSort, start, lastSwappedIndex);
        return lastSwappedIndex;
    }

    private static void swap(int[] arrayToSort, int a, int b) {
        int tmp = arrayToSort[a];
        arrayToSort[a] = arrayToSort[b];
        arrayToSort[b] = tmp;
    }

    public static int selectPiviot(int start, int end) {
        return end; // (start + end) / 2
    }
}
